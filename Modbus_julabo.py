#!/usr/bin/env python3

import os
from configparser import ConfigParser
import mysql.connector
# from datetime import datetime
import time
from pyModbusTCP.client import ModbusClient
from pyModbusTCP import utils

myDir = os.path.dirname(os.path.realpath(__file__))
configFileName = myDir+'/config.ini'

# Check if the configuration file exists
if not os.path.exists(configFileName) :
    print("Config file '%s' does not exist" % configFileName)
    sys.exit(os.EX_CONFIG)

# read configuration file
config = ConfigParser()
config.read(configFileName)

# Get the variables from the config file
# Database write access
dbHost = config['mysql']['host']
dbPort = config['mysql']['port']
dbDatabase = config['mysql']['database']
dbUser = config['mysql']['user']
dbPassword = config['mysql']['password']

julaboHostName = config['julabo']['hostname']
julaboPort = int(config['julabo']['port'])

try:
    # connect to database
    mydb = mysql.connector.connect(
        host=dbHost,
        port=dbPort,
        user=dbUser,
        password=dbPassword,
        database=dbDatabase
    )
except:
    print("Could not connect to database")
    exit(-1)

# Create a cursor object
mycursor = mydb.cursor()

def epoch():
    return int(round(time.time()))

measurement_types = {'setpoint_temp': 1, 'bath_temp': 2}
## Connect to the Julabo

try:
    c = ModbusClient(host=julaboHostName, port=julaboPort, auto_open=True, auto_close=True)
    
except ValueError:
    print("Error with host or port params")

if c.open():
    epoch_timestamp = epoch()
    running_list  = c.read_holding_registers(0, 1) # This is a 16-bit integer holding 0 or 1
    setpoint_temp_list = c.read_holding_registers(2, 2) # These are two 16-bit registers holding a float
    bath_temp_list = c.read_input_registers(10, 2) #to read the bath_temp
else :
    print("There was error opening the modbus client")
    exit(-1)

running  = running_list[0]
setpoint_temp = utils.decode_ieee(utils.word_list_to_long(setpoint_temp_list)[0]) # We need to decode two 16-bit integers -> float

# If am getting issuue here beacuse i get value equals to zero when i try with these two 
bath_temp = utils.decode_ieee(utils.word_list_to_long(bath_temp_list)[0])

# Read the columns where to select the values

# mycursor.execute("SELECT id, name FROM measurement_types WHERE id IN (22, 23)")
# measurement_types = {row[1]: row[0] for row in mycursor.fetchall()}

# If the Chiller is ON 
if (running == 1):
    #print("The chiller is ", end='')
    #print("running, and the setpoint is {:.4f}".format(setpoint_temp)+" C")
    #print("bath_temp is {:.2f}".format(bath_temp)+ " C")
    mycursor.execute("INSERT INTO measurements (value, measurement_type_id, epoch_timestamp) VALUES (%s, %s, %s)", (setpoint_temp, 22, epoch_timestamp))
    mycursor.execute("INSERT INTO measurements (value, measurement_type_id, epoch_timestamp) VALUES (%s, %s, %s)", (bath_temp, 23, epoch_timestamp))

# If the Chiller is Off 
if (running == 0):
    print("not ", end='')
    mycursor.execute("INSERT INTO measurements (value, measurement_type_id, epoch_timestamp) VALUES (%s, %s, %s)", (-999, 22, epoch_timestamp))
    mycursor.execute("INSERT INTO measurements (value, measurement_type_id, epoch_timestamp) VALUES (%s, %s, %s)", (-999, 23, epoch_timestamp))

# Insert the measurements into the database, including the epoch timestamp
mydb.commit()

# Close the Modbus TCP/IP connection and database connection
mycursor.close()
c.close()


