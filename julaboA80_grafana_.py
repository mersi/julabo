from configparser import ConfigParser

import mysql.connector
import serial
import configparser
from datetime import datetime
import os
import logging
import serial
import time


# read configuration file
config = ConfigParser()
config.read('config.ini')

# Get the vareialbes from the config file
# Database write access
dbHost = config['mysql']['host']
dbPort = config['mysql']['port']
dbDatabase = config['mysql']['database']
dbUser = config['mysql']['user']
dbPassword = config['mysql']['password']

#connect to database
mydb = mysql.connector.connect(
host=dbHost,
port=dbPort,
user=dbUser,
password=dbPassword,
database=dbDatabase
)


print(mydb,"is connected")
mycursor = mydb.cursor()
# Create a cursor object
mycursor = mydb.cursor()



def epoch():
    tstamp = int(round(datetime.now().timestamp()))
    return tstamp



# Define the measurement types
measurement_types = {'setpoint_temp': 1, 'bath_temp': 2}

# Connect to the serial port
ser = serial.Serial('/dev/ttyUSB0', 9600, timeout=1)

print("Connected to port")
mycursor.execute("SELECT id, name FROM measurement_types WHERE id IN (22, 23)")
measurement_types = {row[1]: row[0] for row in mycursor.fetchall()}
##Check the Current Mode of the chiller

ser.write("in_mode_05\r\n".encode("utf-8"))
puffer = ser.readline()
print(puffer)

time.sleep(3)
# Turn on the chiller
ser.write("out_mode_05 1\r\n".encode("utf-8"))
print("Turn On the Chiller")
time.sleep(6)


# Set the temperature set point to a range of 18 to 21 degrees Celsius using a for loop



for temperature in range(18, 25):
    # Set the temperature set point
    ser.write("out_sp_00 {}\r".format(temperature).encode())
    time.sleep(600)
    ser.write("in_pv_00?\r".encode())
    temperature1 = float(ser.readline().decode().strip())
    print("Setpoint is {}C, Actual bath temperature is {}C".format(temperature, temperature1))
    # Insert the measurements into the database
    setpoint_temp = temperature
    bath_temp = temperature1
    #mycursor.execute("INSERT INTO measurements (value, measurement_type_id) VALUES (%s, %s, %s)", (setpoint_temp, measurement_types['setpoint_temp']))
    # Get the current epoch timestamp
    epoch_timestamp = epoch()

# Insert the measurements into the database, including the epoch timestamp
    mycursor.execute("INSERT INTO measurements (value, measurement_type_id, epoch_timestamp) VALUES (%s, %s, %s)", (setpoint_temp, measurement_types['setpoint_temp'], epoch_timestamp))

    mycursor.execute("INSERT INTO measurements (value, measurement_type_id, epoch_timestamp) VALUES (%s, %s, %s)", (bath_temp, measurement_types['Bath_temp'],epoch_timestamp ))
    mydb.commit()

print("data stored in daqmonitor")
# Turn off the chiller
ser.write("out_mode_05 0\r\n".encode("utf-8"))
print("Turn off the chiller")
# Close the serial port and database connection
ser.close()
mycursor.close()
mydb.close()


